package com.example.cameraapp.api

data class PostLogin(
    val username: String,
    val password: String
)

data class ResponseLogin(
    val status: Boolean,
    val hostId: String
)

data class PostCard(
    val machineId: String,
    val cardID: String
)

data class ResponseCard(
    val status: Boolean,
    val message: String
)

class Card {
    var code: String = ""
    var count: Int = 0
    var isReadyForApi: Boolean = true
    val suits = arrayOf('c', 'd', 'h', 's')
    val ranks = arrayOf('a', '1', '2', '3', '4', '5', '6', '7', '8', '9', 't', 'j', 'q', 'k')
    val unknowns = arrayOf('m')
}
