import com.example.cameraapp.api.ApiInterface
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val URL = "http://139.59.67.166:2000/api/"

class ApiClient {
    companion object {
        private var retrofit: Retrofit? = null
        fun getApiClient(): Retrofit {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val okHttp: OkHttpClient.Builder =
                OkHttpClient.Builder()
                    .callTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS);
//                .build();
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttp.build())
                    .build()
            }
            return retrofit!!
        }
    }
}


//package com.example.cameraapp.api
//
//import okhttp3.OkHttpClient
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//
//object ApiBuilder {
//    private const val URL = "http://139.59.67.166:2000/api/"
//
//    // create okhttp client
//    private val okHttp: OkHttpClient.Builder = OkHttpClient.Builder()
//
//    // create retrofit builder
//    private  val retrofit =
//        Retrofit.Builder()
//            .baseUrl(URL)
//            .addConverterFactory(GsonConverterFactory.create())
//            .client(okHttp.build())
//            .build()
//    val service = retrofit.create(ApiInterface::class.java)
//    public fun getService() {
//        return retrofit.create(ApiInterface::class.java)
//    }
// create retrofit instance
//    private val retrofit = builder.build()
//    fun <T> ApiService(ServiceType: Class<T>): T {
//        return retrofit.create(ServiceType)
//    }
//}