package com.example.cameraapp

import android.Manifest
import android.content.Context
import android.widget.Toast

object Constants {
    const val TAG = "cameraX"
    const val machinId = "machin45"
    const val th1_key = "threshold_key_one"
    const val th2_key = "threshold_key_two"
    const val zoom_key = "zoom_key"
    const val host_key = "host_id_key"
    const val shared_preferences ="shared_preferences_key"
    const val REQUEST_CODE_PERMISSIONS = 123
    val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
}

fun Context.showMessage(msg: String, len: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, msg, len).show()
}