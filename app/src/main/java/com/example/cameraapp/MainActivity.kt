package com.example.cameraapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.camera.core.Camera
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import com.example.cameraapp.api.*
import com.example.cameraapp.databinding.ActivityMainBinding
import com.example.cameraapp.util.ConvertToBitmap
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {
    private lateinit var alerBuilder: AlertDialog.Builder
    private var mCard: Card = Card()
    private lateinit var binding: ActivityMainBinding
    private var imageAnalyzer: ImageAnalysis? = null
    private var camerax: Camera? = null
    private lateinit var cameraExecutor: ExecutorService
    private var minZoom: Float? = null
    private var maxZoom: Float? = null
    private var controlView: Boolean = false

    private var th1: Int? = null
    private var th2: Int? = null
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var hostId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        hostId = intent.getStringExtra(Constants.host_key).toString()
        loadPreProcessImage()

        sharedPreferences = getPreferences(Context.MODE_PRIVATE)
        th1 = sharedPreferences.getInt(Constants.th1_key, 255)
        th2 = sharedPreferences.getInt(Constants.th2_key, 255)

        th1?.let {
            binding.textThreshOne.setText("th1 : $th1")
            binding.thresholdone.setProgress(it)
            setThresholdOne(it)
        }

        th2?.let {
            binding.textThreshTwo.setText("th2 : $th2")
            binding.thresholtwo.setProgress(it)
            setThresholdTwo(it)
        }

        if (allPermissionGranted()) {
            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                this, Constants.REQUIRED_PERMISSIONS, Constants.REQUEST_CODE_PERMISSIONS
            )
        }

        cameraExecutor = Executors.newSingleThreadExecutor()

        binding.buttonZoomIn.setOnClickListener {
            Log.d(Constants.TAG, "in")
            camerax?.let { camera ->
                var zoomRatio: Float = camera.cameraInfo.zoomState.value!!.zoomRatio
                if (zoomRatio < maxZoom!!) {
                    camera.cameraControl.setZoomRatio(zoomRatio + minZoom!!)
                    sharedPreferences.edit() {
                        putFloat(Constants.zoom_key, zoomRatio + minZoom!!)
                    }
                }
            }
        }
        binding.buttonZoomOut.setOnClickListener {
            Log.d(Constants.TAG, "out")
            camerax?.let { camera ->
                var zoomRatio: Float = camera.cameraInfo.zoomState.value!!.zoomRatio
                if (zoomRatio > minZoom!!) {
                    camera.cameraControl.setZoomRatio(zoomRatio - minZoom!!)
                    sharedPreferences.edit() {
                        putFloat(Constants.zoom_key, zoomRatio - minZoom!!)
                    }
                }
            }
        }
        binding.buttonControl.setOnClickListener {
            controlView = !controlView
            setControl(controlView)
            if (controlView) {
                binding.controlContainer.visibility = View.VISIBLE
            } else {
                binding.controlContainer.visibility = View.GONE
            }
        }
        binding.thresholdone.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                th1 = progress
                binding.textThreshOne.setText("th1 : $th1")
                setThresholdOne(progress)
                sharedPreferences.edit {
                    putInt(Constants.th1_key, progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        }
        )
        binding.thresholtwo.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                th2 = progress
                binding.textThreshTwo.setText("th2 : $th2")
                setThresholdTwo(progress)
                sharedPreferences.edit {
                    putInt(Constants.th2_key, progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        }
        )
        alerBuilder = AlertDialog.Builder(this)
        alerBuilder.setTitle("Are you sure!")
        alerBuilder.setMessage("Do you want to logout")
        alerBuilder.setPositiveButton("Yes") { dialogInterface: DialogInterface, i: Int ->
            var sp: SharedPreferences =
                getSharedPreferences(Constants.shared_preferences, Context.MODE_PRIVATE)
            sp.edit() {
                putString(Constants.host_key, "")
            }
            var intent = Intent(this, SplashActivity::class.java)
            intent.flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
        alerBuilder.setNegativeButton("No") { dialogInterface: DialogInterface, i: Int ->
            Log.d(Constants.TAG, "No")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var itemView = item.itemId
        when (itemView) {
            R.id.logout -> alerBuilder.show()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadPreProcessImage() {
        //setPreProcess Suit type 0/1=>suit/rank
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.s1), 0, 0)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.s2), 0, 1)
//        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.s3),0,2)
//        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.s4),0,3)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r1), 1, 0)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r2), 1, 1)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r3), 1, 2)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r4), 1, 3)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r5), 1, 4)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r6), 1, 5)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r7), 1, 6)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r8), 1, 7)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r9), 1, 8)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r10), 1, 9)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r11), 1, 10)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r12), 1, 11)
        setPreProcessImage(BitmapFactory.decodeResource(this.resources, R.drawable.r13), 1, 12)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == Constants.REQUEST_CODE_PERMISSIONS) {
            if (allPermissionGranted()) {
                startCamera()
            } else {
                Toast.makeText(this, "Permission not granted", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }

    private fun allPermissionGranted() =
        Constants.REQUIRED_PERMISSIONS.all {
            ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
        }

    @SuppressLint("UnsafeOptInUsageError")
    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
            imageAnalyzer = ImageAnalysis.Builder()
                .setTargetResolution(Size(1280, 720))
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build().also {
                    it.setAnalyzer(cameraExecutor, CustomImageAnalyzer(this).apply {
                        setOnProcessImage(object : CustomImageAnalyzer.ProcessImage {
                            override fun setOnProcessImage(mImage: Bitmap) {
                                runOnUiThread {
                                    try {
                                        var mCode: String = detectCard(mImage, mImage)
                                        if (!controlView) {
                                            detectionHandler(mCode)
                                        }
                                        binding.imageView.setImageBitmap(mImage)
                                    } catch (e: Exception) {
                                        Log.d(Constants.TAG, "binding", e)
                                    }
                                }
                            }
                        })
                    })
                }
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
            try {
                cameraProvider.unbindAll()
                camerax = cameraProvider.bindToLifecycle(this, cameraSelector, imageAnalyzer)
                camerax?.let { camera ->
                    camera.cameraInfo.zoomState.observe(
                        this,
                        androidx.lifecycle.Observer { ZoomState ->
                            binding.textZoom.setText("${ZoomState.zoomRatio.toString()}f")
                        })
                    minZoom = camera.cameraInfo.zoomState.value?.minZoomRatio
                    maxZoom = camera.cameraInfo.zoomState.value?.maxZoomRatio
                    sharedPreferences.getFloat(Constants.zoom_key, minZoom!!)?.let {
                        camera.cameraControl.setZoomRatio(it)
                    }
                }
            } catch (e: Exception) {
                Log.d(Constants.TAG, "start camera failed", e)
            }
        }, ContextCompat.getMainExecutor(this))
    }

    private class CustomImageAnalyzer(mContext: Context) : ImageAnalysis.Analyzer {
        private lateinit var mProcessImage: ProcessImage
        private var mConvertToBitmap = ConvertToBitmap(mContext)

        @SuppressLint("UnsafeOptInUsageError")
        override fun analyze(imageProxy: ImageProxy) {
            val rotationDegrees = imageProxy.imageInfo.rotationDegrees
            val image = imageProxy.image
            if (image != null) {
                val mImage: Bitmap? = mConvertToBitmap.toBitmap(imageProxy)
                if (mImage != null) {
                    mProcessImage.setOnProcessImage(mImage)
                }
            }
            Thread.sleep(100)
            imageProxy.close()
        }

        interface ProcessImage {
            fun setOnProcessImage(mImage: Bitmap)
        }

        fun setOnProcessImage(mProcessImage: ProcessImage) {
            this.mProcessImage = mProcessImage
        }
    }

    private fun detectionHandler(mCode: String) {
        val mCodeChar: CharArray = mCode.toCharArray()
        if (mCard.suits.contains(mCodeChar[0])
            && mCard.ranks.contains(mCodeChar[1]) && mCard.isReadyForApi
        ) {
            //Card detected
            if (mCard.code != mCode) {
                mCard.code = mCode
                mCard.count = 1
            } else {
                mCard.count++
                if (mCard.count >= 5) {
                    binding.textCode.setText(mCode)
                    mCard.code = ""
                    mCard.count = 0
                    mCard.isReadyForApi = false //ready to detect no card
                    httpRequestHandler(mCode)
                    showMessage("card detected :: $mCode")
                    Log.d(Constants.TAG, "Card::Detected : ${mCode}")
                }
            }
        } else if (mCard.unknowns.contains(mCodeChar[0]) && !mCard.isReadyForApi) {
            //Card Not detected
            if (mCard.code != "m") {
                mCard.code = "m"
                mCard.count = 1
            } else {
                mCard.count++
                if (mCard.count >= 30) {
                    binding.textCode.setText("detecting...")
                    mCard.code = ""
                    mCard.count = 0
                    mCard.isReadyForApi = true // ready to detect next card
                    showMessage("Ready to detect new card")
                }
            }
        }
    }

    private fun httpRequestHandler(cardId: String) {
        val post = PostCard(hostId, cardId)
        val api: ApiInterface = ApiClient.getApiClient().create(ApiInterface::class.java)
        val call: Call<ResponseCard> = api.postCard(post)
        call.enqueue(
            object : Callback<ResponseCard> {
                override fun onResponse(
                    call: Call<ResponseCard>,
                    response: Response<ResponseCard>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let { responseCard ->
                            if (responseCard.status) {
                                Log.d(Constants.TAG, responseCard.toString())
                                showMessage("$cardId :: sent to server successfully")
                            } else {
                                Log.d(Constants.TAG, "api error")
                                showMessage("Api response status false")
                            }
                        }
                    } else {
                        response.errorBody()?.let {
                            Log.d(Constants.TAG, it.string())
                            showMessage(it.string())
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseCard>, t: Throwable) {
                    Log.d(Constants.TAG, "sdf")
                    showMessage("onFailure call")
                }
            }
        )
    }

    private external fun setPreProcessImage(bitmapIn: Bitmap, type: Int, index: Int)
    private external fun detectCard(bitmapIn: Bitmap, bitmapOut: Bitmap): String
    private external fun setControl(control: Boolean)
    private external fun setThresholdOne(th: Int)
    private external fun setThresholdTwo(th: Int)

    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }

}
