package com.example.cameraapp

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.edit
import com.example.cameraapp.api.ApiInterface
import com.example.cameraapp.api.PostLogin
import com.example.cameraapp.api.ResponseLogin
import com.example.cameraapp.databinding.ActivityLoginBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private lateinit var binding: ActivityLoginBinding
private lateinit var sharedPreferences: SharedPreferences

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPreferences = getSharedPreferences(Constants.shared_preferences, Context.MODE_PRIVATE)
        binding.buttonLogin.setOnClickListener {
            val username: String = binding.editTextUserName.text.toString()
            val password: String = binding.editTextPassword.text.toString()
            if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
                showMessage("Please enter valid username and password")
            } else {
                toggleProgressBar(View.VISIBLE)
                val post = PostLogin(username, password)
                val api: ApiInterface = ApiClient.getApiClient().create(ApiInterface::class.java)
                val call: Call<ResponseLogin> = api.login(post)
                call.enqueue(
                    object : Callback<ResponseLogin> {
                        override fun onResponse(
                            call: Call<ResponseLogin>,
                            response: Response<ResponseLogin>
                        ) {
                            toggleProgressBar(View.GONE)
                            if (response.isSuccessful) {
                                response.body()?.let { response ->
                                    if (response.status) {
                                        Log.d(Constants.TAG, response.toString())
                                        if (!response.hostId.isNullOrEmpty()) {
                                            loginHandler(response.hostId)
                                        } else {
                                            showMessage("unauthorized user")
                                        }
                                    } else {
                                        Log.d(Constants.TAG, "api error")
                                        showMessage("something went wrong")
                                    }
                                }
                            } else {
                                response.errorBody()?.let {
                                    Log.d(Constants.TAG, it.string())
                                    showMessage(it.string())
                                }
                            }
                        }

                        override fun onFailure(call: Call<ResponseLogin>, t: Throwable) {
                            toggleProgressBar(View.GONE)
                            Log.d(Constants.TAG, "sdf")
                            showMessage("onFailure call")
                        }
                    }
                )
            }
        }
    }

    private fun loginHandler(hostId: String) {
        sharedPreferences.edit() {
            putString(Constants.host_key, hostId)
        }
        var intent = Intent(this, MainActivity::class.java)
        intent.putExtra(Constants.host_key, hostId)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

    private fun toggleProgressBar(visibility: Int) {
        binding.progressBar.visibility = visibility
    }
}