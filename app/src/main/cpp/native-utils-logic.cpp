#include "native-utils-logic.h"

Mat suits[2];
Mat ranks[13];

void consoleLog(const char *str) {
    __android_log_print(ANDROID_LOG_INFO, APPNAME, "%s", str);
}

void SetPreProcessImage(Mat &ppImg, int type, int index) {
    cvtColor(ppImg, ppImg, COLOR_BGR2GRAY);
    if (type == 0) {
        suits[index] = ppImg;
    }
    if (type == 1) {
        ranks[index] = ppImg;
    }
}

float distance(int x1, int y1, int x2, int y2) {
    return sqrt(pow(x2 - x1, 2) +
                pow(y2 - y1, 2) * 1.0);
}

Mat getDil(Mat img, int th1, int th2) {
    cvtColor(img, img, COLOR_BGR2GRAY);
    GaussianBlur(img, img, Size(3, 3), 3, 0);
    Canny(img, img, th1, th2);
    Mat kernal = getStructuringElement(MORPH_RECT, Size(3, 3));
    dilate(img, img, kernal);
    return img;
}

vector<vector<Point>> getCountours(Mat &img, Mat imgDil) {
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    findContours(imgDil, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
//    drawContours(img, contours, -1, Scalar(180, 25, 25), 2);
    return contours;
}

vector<Point> getCardCountours(vector<vector<Point>> contours) {
    vector<vector<Point>> conPoly(contours.size());
    vector<Rect> boundRect(contours.size());
    int j = 0;
    int jarea = 0;
    for (int i = 0; i < contours.size(); i++) {
        int area = contourArea(contours[i]);
        float peri = arcLength(contours[i], true);
        approxPolyDP(contours[i], conPoly[i], 0.02 * peri, true);
        if (jarea < area) {
            j = i;
            jarea = area;
        }
        //cout << "area of contours " << i << " : " << area << endl;
    }
    //drawContours(img, conPoly, j, Scalar(180, 50, 25), 2);
    //drawContours(img, contours, j, Scalar(180, 25, 25), 2);
    //return contours[j];
//    string msg = "area of main contours : " + to_string(jarea);
//    consoleLog(msg.c_str());
    return conPoly[j];
}

Rect getBoundRect(Mat img, vector<Point> contour) {
    vector<Point> conPoly;
    Rect boundRect;
    float peri = arcLength(contour, true);
    approxPolyDP(contour, conPoly, 0.02 * peri, true);
    boundRect = boundingRect(conPoly);
    //rectangle(img, boundRect.tl(), boundRect.br(), Scalar(13, 251, 227), 2);
    return boundRect;
}

Mat detectCardImage(Mat img, vector<Point> contour) {
    float d1 = distance(contour[0].x, contour[0].y, contour[1].x, contour[1].y);
    float d2 = distance(contour[1].x, contour[1].y, contour[2].x, contour[2].y);
    float width, height;
    Point2f ed0, ed1, ed2, ed3;
    Mat matrix, imgCard;
    if (d1 < d2) {
        width = d1;
        height = d2;
        ed0 = {float(contour[1].x), float(contour[1].y)};
        ed1 = {float(contour[0].x), float(contour[0].y)};
        ed2 = {float(contour[2].x), float(contour[2].y)};
        ed3 = {float(contour[3].x), float(contour[3].y)};
    } else {
        width = d2;
        height = d1;
        ed2 = {float(contour[1].x), float(contour[1].y)};
        ed0 = {float(contour[0].x), float(contour[0].y)};
        ed3 = {float(contour[2].x), float(contour[2].y)};
        ed1 = {float(contour[3].x), float(contour[3].y)};
    }
    Point2f src[4] = {ed0, ed1, ed2, ed3};
    Point2f dst[4] = {{0.0f,  0.0f},
                      {width, 0.0f},
                      {0.0f,  height},
                      {width, height}};
    matrix = getPerspectiveTransform(src, dst);
    warpPerspective(img, imgCard, matrix, Point(width, height));
    resize(imgCard, imgCard, Size(220, 330), INTER_LINEAR);
    return imgCard;
}

Mat imageWrap(Mat img, Rect boundRect) {
    Mat matrix, imgwrap;
    float x = boundRect.x,
            y = boundRect.y,
            w = boundRect.width,
            h = boundRect.height;
    Point2f src[4] = {{x,     y},
                      {x + w, y},
                      {x,     y + h},
                      {x + w, y + h}};
    Point2f dst[4] = {{0.0f, 0.0f},
                      {w,    0.0f},
                      {0.0f, h},
                      {w,    h}};
    matrix = getPerspectiveTransform(src, dst);
    warpPerspective(img, imgwrap, matrix, Point(w, h));
    if (w > h) { rotate(imgwrap, imgwrap, ROTATE_90_CLOCKWISE); }
    return imgwrap;
}

string getSuit(int value) {
    switch (value) {
        case 1:
            return "c";
        case 2:
            return "d";
        case 3:
            return "h";
        case 4:
            return "s";
        default:
            return "u";
    }
}

string getRank(int value) {
    switch (value) {
        case 1:
            return "a";
        case 2:
            return "2";
        case 3:
            return "3";
        case 4:
            return "4";
        case 5:
            return "5";
        case 6:
            return "6";
        case 7:
            return "7";
        case 8:
            return "8";
        case 9:
            return "9";
        case 10:
            return "t";
        case 11:
            return "j";
        case 12:
            return "q";
        case 13:
            return "k";
        default:
            return "u";
    }
}

double compareImage(Mat a, Mat b) {
    Mat img1;
    Mat img2 = b;
    resize(a, img1, img2.size());
    double error, result = 2, area;
    try {
        area = double(img2.rows) * double(img2.cols);
        error = norm(img1, img2);
        result = error / area;
    }
    catch (Exception &e) {
        consoleLog("compare error");
    }
    return result;
}

int detectSuits(Mat imgSuit) {
    double result = 2;
    int suitNo = 0;
    for (int i = 0; i < 2; i++) {
        double currentResult = compareImage(imgSuit, suits[i]);
        if (result > currentResult) {
            result = currentResult;
            suitNo = i + 1;
        }
    }
    return suitNo;
}

int detectRanks(Mat imgRank) {
    double result = 2;
    int rankNo = 0;
    for (int i = 0; i < 13; i++) {
        double currentResult = compareImage(imgRank, ranks[i]);
        if (result > currentResult) {
            result = currentResult;
            rankNo = i + 1;
        }
    }
    return rankNo;
}