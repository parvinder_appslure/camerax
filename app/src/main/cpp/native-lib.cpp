#include <jni.h>
#include "native-utils.h"
#include "native-utils-logic.h"

using namespace std;
using namespace cv;

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_cameraapp_MainActivity_detectCard(JNIEnv *env, jobject thiz, jobject bitmap_in,
                                                   jobject bitmap_out) {
    Mat src;
    bitmapToMat(env, bitmap_in, src, false);
    String msg = DetectCard(src);
    matToBitmap(env, src, bitmap_out, false);
    return env->NewStringUTF(msg.c_str());
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_cameraapp_MainActivity_setPreProcessImage(JNIEnv *env, jobject thiz,
                                                           jobject bitmap_in, jint type,
                                                           jint index) {
    Mat ppImg;
    bitmapToMat(env, bitmap_in, ppImg, false);
    SetPreProcessImage(ppImg, type, index);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_cameraapp_MainActivity_setControl(JNIEnv *env, jobject thiz, jboolean control) {
    SetControl(control);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_cameraapp_MainActivity_setThresholdOne(JNIEnv *env, jobject thiz, jint th) {
    SetThresholdOne(th);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_cameraapp_MainActivity_setThresholdTwo(JNIEnv *env, jobject thiz, jint th) {
    SetThresholdTwo(th);
}