#include "native-utils.h"

void bitmapToMat(JNIEnv *env, jobject bitmap, Mat &dst, jboolean needUnPremultiplyAlpha) {
    AndroidBitmapInfo info;
    void *pixels = 0;

    try {
        CV_Assert(AndroidBitmap_getInfo(env, bitmap, &info) >= 0);
        CV_Assert(info.format == ANDROID_BITMAP_FORMAT_RGBA_8888 ||
                  info.format == ANDROID_BITMAP_FORMAT_RGB_565);
        CV_Assert(AndroidBitmap_lockPixels(env, bitmap, &pixels) >= 0);
        CV_Assert(pixels);
        dst.create(info.height, info.width, CV_8UC4);
        if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
            Mat tmp(info.height, info.width, CV_8UC4, pixels);
            if (needUnPremultiplyAlpha) cvtColor(tmp, dst, COLOR_mRGBA2RGBA);
            else tmp.copyTo(dst);
        } else {
            // info.format == ANDROID_BITMAP_FORMAT_RGB_565
            Mat tmp(info.height, info.width, CV_8UC2, pixels);
            cvtColor(tmp, dst, COLOR_BGR5652RGBA);
        }
        AndroidBitmap_unlockPixels(env, bitmap);
        return;
    } catch (const cv::Exception &e) {
        AndroidBitmap_unlockPixels(env, bitmap);
        jclass je = env->FindClass("java/lang/Exception");
        env->ThrowNew(je, e.what());
        return;
    } catch (...) {
        AndroidBitmap_unlockPixels(env, bitmap);
        jclass je = env->FindClass("java/lang/Exception");
        env->ThrowNew(je, "Unknown exception in JNI code {nBitmapToMat}");
        return;
    }
}

void matToBitmap(JNIEnv *env, Mat src, jobject bitmap, jboolean needPremultiplyAlpha) {
    AndroidBitmapInfo info;
    void *pixels = 0;

    try {
        CV_Assert(AndroidBitmap_getInfo(env, bitmap, &info) >= 0);
        CV_Assert(info.format == ANDROID_BITMAP_FORMAT_RGBA_8888 ||
                  info.format == ANDROID_BITMAP_FORMAT_RGB_565);
        CV_Assert(src.dims == 2 && info.height == (uint32_t) src.rows &&
                  info.width == (uint32_t) src.cols);
        CV_Assert(src.type() == CV_8UC1 || src.type() == CV_8UC3 || src.type() == CV_8UC4);
        CV_Assert(AndroidBitmap_lockPixels(env, bitmap, &pixels) >= 0);
        CV_Assert(pixels);
        if (info.format == ANDROID_BITMAP_FORMAT_RGBA_8888) {
            Mat tmp(info.height, info.width, CV_8UC4, pixels);
            if (src.type() == CV_8UC1) {
                cvtColor(src, tmp, COLOR_GRAY2RGBA);
            } else if (src.type() == CV_8UC3) {
                cvtColor(src, tmp, COLOR_RGB2RGBA);
            } else if (src.type() == CV_8UC4) {
                if (needPremultiplyAlpha) cvtColor(src, tmp, COLOR_RGBA2mRGBA);
                else src.copyTo(tmp);
            }
        } else {
            // info.format == ANDROID_BITMAP_FORMAT_RGB_565
            Mat tmp(info.height, info.width, CV_8UC2, pixels);
            if (src.type() == CV_8UC1) {
                cvtColor(src, tmp, COLOR_GRAY2BGR565);
            } else if (src.type() == CV_8UC3) {
                cvtColor(src, tmp, COLOR_RGB2BGR565);
            } else if (src.type() == CV_8UC4) {
                cvtColor(src, tmp, COLOR_RGBA2BGR565);
            }
        }
        AndroidBitmap_unlockPixels(env, bitmap);
        return;
    } catch (const cv::Exception &e) {
        AndroidBitmap_unlockPixels(env, bitmap);
        jclass je = env->FindClass("java/lang/Exception");
        env->ThrowNew(je, e.what());
        return;
    } catch (...) {
        AndroidBitmap_unlockPixels(env, bitmap);
        jclass je = env->FindClass("java/lang/Exception");
        env->ThrowNew(je, "Unknown exception in JNI code {nMatToBitmap}");
        return;
    }
}

bool controlView = false;
int th1 = 255, th2 = 255;

void SetControl(bool control) {
    controlView = control;
}

void SetThresholdOne(int th) {
    th1 = th;
}

void SetThresholdTwo(int th) {
    th2 = th;
}

String DetectCard(Mat &src) {
    Mat mat = src;
    Mat matDil = getDil(mat, th1, th2);
    if (controlView) {
        src = matDil;
        return "md";
    }
    vector<vector<Point>> mainContours = getCountours(mat, matDil);
    int mcLen = (int) mainContours.size();
//    string msg = "mc len : " + to_string(mcLen);
//    consoleLog(msg.c_str());
    if (mcLen == 0) {
//        consoleLog("no detection");
        src = mat;
        return "m0";
    }
    vector<Point> cardCountor;
    cardCountor = getCardCountours(mainContours); //filter max area
    int area = contourArea(cardCountor);
    int ccsize = cardCountor.size();
    if (area < 30000 || ccsize != 4) {
//        consoleLog(" no detection");
//        cout << "detected area" << area << endl;
//        cout << "detected area points" << cardCountor.size() << endl;
//        return 0;
//        detectText = "area : " + to_string(area) + " size : " + to_string(ccsize);
        return "m1";
    }
    Rect cardBoundRect = getBoundRect(mat, cardCountor); // card detection rectangle wrap
    Mat imgCard = detectCardImage(mat, cardCountor); //card crop

    if (imgCard.empty()) {
        src = mat;
        return "m2";
    }
//    resize(imgCard, imgCard, mat.size());
//    mat = imgCard;
// check deckImage

    Mat imgSuitRank = imgCard(Range(5, 160), Range(3, 53)); // card suit and rank area crop
    Mat imgSuitRankDil = getDil(imgSuitRank, 24, 75);

//    resize(imgSuitRankDil, imgSuitRankDil, mat.size());
//    src = imgSuitRankDil;
//    return "imgSuitRank";
// check deckImage
    vector<vector<Point>> subContours = getCountours(imgSuitRank, imgSuitRankDil);
    for (int i = 0; i < subContours.size(); i++) {
        int area = contourArea(subContours[i]);
        if (area < 300) {
            subContours.erase(
                    subContours.erase(subContours.begin() + i, subContours.begin() + i));
        }
    }
    int scLen = (int) subContours.size();
    if (scLen == 0) {
        src = mat;
        return "m3";
    }

    int rankValue = 0, suitValue = 0;
    String detectCode;
    bool flagTen = false;
    for (int i = 0; i < scLen; i++) {
        Rect boundRect = getBoundRect(imgSuitRank, subContours[i]);
        Mat imgwrap = imageWrap(imgSuitRank, boundRect);
        cvtColor(imgwrap, imgwrap, COLOR_BGR2GRAY);
        if (boundRect.y < 30) { // identify suit and rank
            if (imgwrap.cols > 15) {
                rankValue = detectRanks(imgwrap);
//                resize(imgwrap, imgwrap, mat.size());
//                src = imgwrap;
//                return "rank";
            } else {
                int area = boundRect.area();
                int height = boundRect.height;
                int width = boundRect.width;
                if (area > 350 && height > 50 && width > 6 && scLen == 3) {
                    flagTen = true;
                }
            }
        } else {
            suitValue = detectSuits(imgwrap);
//            resize(imgwrap, imgwrap, mat.size());
//            src = imgwrap;
//            return "suit";
        }
    }
    if (flagTen) {
        rankValue = 10;
    }
    detectCode = getSuit(suitValue) + getRank(rankValue);
    Point p1(cardBoundRect.tl().x - 10, cardBoundRect.tl().y - 10);
    Point p2(cardBoundRect.br().x + 10, cardBoundRect.br().y + 10);
    rectangle(mat, p1, p2, Scalar(13, 251, 227), 2);
    src = mat;
    return detectCode;
}