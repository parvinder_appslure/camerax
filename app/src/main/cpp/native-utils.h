#pragma once
#include <jni.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include "android/bitmap.h"
#include "native-utils-logic.h"
using namespace std;
using namespace cv;

void bitmapToMat(JNIEnv *env, jobject bitmap, Mat &dst, jboolean needUnPremultiplyAlpha);
void matToBitmap(JNIEnv *env, Mat src, jobject bitmap, jboolean needPremultiplyAlpha);
void SetControl(bool control);
void SetThresholdOne(int th);
void SetThresholdTwo(int th);
String DetectCard(Mat &src);
