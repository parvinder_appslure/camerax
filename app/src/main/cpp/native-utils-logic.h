#pragma once

#include <android/log.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#define APPNAME "cameraX_card"

using namespace std;
using namespace cv;

void consoleLog(const char *str);

void SetPreProcessImage(Mat &ppImg, int type, int index);

Mat getDil(Mat mat, int th1, int th2);

vector<vector<Point>> getCountours(Mat &img, Mat imgDil);

vector<Point> getCardCountours(vector<vector<Point>> contours);

Rect getBoundRect(Mat img, vector<Point> contour);

Mat detectCardImage(Mat img, vector<Point> contour);

Mat imageWrap(Mat img, Rect boundRect);

string getSuit(int value);

string getRank(int value);

double compareImage(Mat a, Mat b);

int detectSuits(Mat imgSuit);

int detectRanks(Mat imgRank);
